## maven使用 

  * 统一开发规范工具（代码放哪里，配置文件放哪里，测试用例放哪里...）
  * 统一管理jar包

### 1 maven常用命令

  * maven -version  版本查看
  * mvn compile  编译,将Java 源程序编译成 class 字节码文件
  * mvn package 打包（war或jar），如果没有编译包含执行compile
  * mvn test  执行测试用例,并生成测试报告
  * maven install 打包之后，上传到本地仓库
  * mvn clean 清空之前编译和打包的内容
  * mvn package -Dmaven.test.skip=true  打包跳过测试test
  * mvn clean compile package 先清空，再编译，再打包
  * mvn clean install -Dmaven.test.skip=true 跳过测试执行
  * mvn clean deploy 将包上传到远程仓库，地址可配
  

### 2 pom文件配置说明

#### 2.1 version 版本命名(0.0.1.SNAPSHOT)
  
  * 第一个数字表示大的版本
  * 第二个数字表示整个项目的分支
  * 第三个数字表示项目分支下的细化分支
  * SNAPSHOT：版本快照
  * RELEASE：发布版本
  * BEAT：稳定版本

  ```
  eg：
  开发一个分支功能:
  1.1.1. SNAPSHOT —> 1.1.1. BEAT —> 1.1.1. RELEASE
  同时开发另一个分支功能：
  1.2.1. SNAPSHOT —> 1.2.1. BEAT —> 1.2.1. RELEASE
  两个功能并行开发，最后将两个版本合并:
  1.3.1. SNAPSHOT —> 1.3.1. BEAT —> 1.3.1. RELEASE
  ```

#### 2.2 scope：jar包被使用的范围

  * test: test测试命令执行时有效
  * compile：默认值，编译和运行时都会有效
  * provided：编译时有效，打包时无效
  * runtime：编译时无效，打包时有效

#### 2.3 modelVersion

  * 指定了当前Maven模型的版本号，对于Maven2和Maven3来说，它只能是4.0.0

#### 2.4 groupId

  * 顾名思义，这个应该是公司名或是组织名。一般来说groupId是由三个部分组成，每个部分之间以"."分隔，第一部分是项目用途，比如用于商业的就是"com"，用于非营利性组织的就是"org"；第二部分是公司名，比如"tengxun"、"baidu"、"alibaba"；第三部分是你的项目名

#### 2.5 artifactId

  * 可以认为是Maven构建的项目名，比如你的项目中有子项目，就可以使用"项目名-子项目名"的命名方式

#### 2.6 package

  * 项目打包的类型，可以使jar、war、rar、ear、pom，默认是jar

#### 2.7 dependencies和dependency

  * 添加项目依赖的jar包

#### 2.8 properties

  * properties是用来定义一些配置属性的，例如project.build.sourceEncoding（项目构建源码编码方式），可以设置为UTF-8，防止中文乱码，也可定义相关构建版本号，便于日后统一升级。
  
#### 2.9 build

  * build表示与构建相关的配置，比如build下有finalName，表示的就是最终构建之后的名称

#### 2.10 modules 聚合

  * 在parent父项目中，module是父项目中包含了哪些子模块项目
  * 引用的模块中包含的jar包有多个版本，那么哪个模块在前，就使用哪个模块下的版本
  * 最终D依赖于c1.2，跟依赖模块的顺序没有关系，而是选择依赖层级短的那个模块下的版本</br>
  
  ![cmd-markdown-logo](./images/module.png)

### 3 maven jar包下载地址走向

  * 阿里云和osChina是maven服务在中国的镜像站点

![cmd-markdown-logo](./images/maven.png)

  * 镜像站点配置

 ![cmd-markdown-logo](./images/镜像站点配置.png)
