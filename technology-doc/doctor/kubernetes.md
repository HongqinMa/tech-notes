1.几个重要概念

Cluster： Cluster是计算、存储和网络资源的集合，kubernetes利用这些资源运行各种基于容器的应用；

Master： 是Cluster的大脑，它的主要职责是调度，即决定将应用放在哪里运行。Master运行linux操作系统，可以是物理机或者虚拟机。为了实现高可用，可以运行多个Master。
 
Node： 职责是运行容器应用。Node由Master管理，Node负责监控并汇报容器的状态，并根据Master的要求管理容器的生命周期。Node运行在Linux操作系统，可以是物理机或者是虚拟机。

Pod： Pod是kubernetes的最小工作单元。每个Pod包含一个或多个容器。Pod中的容器会作为一个整体被Master调度到一个Node上运行。


2.Kubernetes 引入 Pod 主要基于下面两个目的：

可管理性。
有些容器天生就是需要紧密联系，一起工作。Pod 提供了比容器更高层次的抽象，将它们封装到一个部署单元中。Kubernetes 以 Pod 为最小单位进行调度、扩展、共享资源、管理生命周期。
通信和资源共享。
Pod 中的所有容器使用同一个网络 namespace，即相同的 IP 地址和 Port 空间。它们可以直接用 localhost 通信。同样的，这些容器可以共享存储，当 Kubernetes 挂载 volume 到 Pod，本质上是将 volume 挂载到 Pod 中的每一个容器。

3.Pods有两种使用方式
运行单一容器。
one-container-per-Pod 是 Kubernetes 最常见的模型，这种情况下，只是将单个容器简单封装成 Pod。即便是只有一个容器，Kubernetes 管理的也是 Pod 而不是直接管理容器。
运行多个容器。
但问题在于：哪些容器应该放到一个 Pod 中？ 
答案是：这些容器联系必须 非常紧密，而且需要 直接共享资源。



Controller：
Kubernetes 通常不会直接创建 Pod，而是通过 Controller 来管理 Pod 的。Controller 中定义了 Pod 的部署特性，比如有几个副本，在什么样的 Node 上运行等。为了满足不同的业务场景，Kubernetes 提供了多种 Controller，包括 Deployment、ReplicaSet、DaemonSet、StatefuleSet、Job 等，我们逐一讨论。

Deployment：
Deployment 是最常用的 Controller，比如前面在线教程中就是通过创建 Deployment 来部署应用的。Deployment 可以管理 Pod 的多个副本，并确保 Pod 按照期望的状态运行。

ReplicaSet 实现了 Pod 的多副本管理。使用 Deployment 时会自动创建 ReplicaSet，也就是说 Deployment 是通过 ReplicaSet 来管理 Pod 的多个副本，我们通常不需要直接使用 ReplicaSet。

DaemonSet 用于每个 Node 最多只运行一个 Pod 副本的场景。正如其名称所揭示的，DaemonSet 通常用于运行 daemon。

StatefuleSet 能够保证 Pod 的每个副本在整个生命周期中名称是不变的。而其他 Controller 不提供这个功能，当某个 Pod 发生故障需要删除并重新启动时，Pod 的名称会发生变化。同时 StatefuleSet 会保证副本按照固定的顺序启动、更新或者删除。

Job 用于运行结束就删除的应用。而其他 Controller 中的 Pod 通常是长期持续运行。
Service 
Deployment 可以部署多个副本，每个 Pod 都有自己的 IP，外界如何访问这些副本呢？
通过 Pod 的 IP 吗？
要知道 Pod 很可能会被频繁地销毁和重启，它们的 IP 会发生变化，用 IP 来访问不太现实。
答案是 Service。
Kubernetes Service 定义了外界访问一组特定 Pod 的方式。Service 有自己的 IP 和端口，Service 为 Pod 提供了负载均衡。
Kubernetes 运行容器（Pod）与访问容器（Pod）这两项任务分别由 Controller 和 Service 执行。
Namespace
如果有多个用户或项目组使用同一个 Kubernetes Cluster，如何将他们创建的 Controller、Pod 等资源分开呢？
答案就是 Namespace。
Namespace 可以将一个物理的 Cluster 逻辑上划分成多个虚拟 Cluster，每个 Cluster 就是一个 Namespace。不同 Namespace 里的资源是完全隔离的。
Kubernetes 默认创建了两个 Namespace。

default -- 创建资源时如果不指定，将被放到这个 Namespace 中。
kube-system -- Kubernetes 自己创建的系统资源将放到这个 Namespace 中。

东四环中路78号大成国际B1座9层9B22(大望路双井国贸四惠大郊亭)


http://www.cnblogs.com/CloudMan6/p/8252204.html
